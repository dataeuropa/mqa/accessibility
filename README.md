# Metrics Accessibility

## Table of Contents
1. [Build](#build)
1. [Run](#run)
1. [Docker](#docker)
1. [Configuration](#configuration)
    1. [Environment](#environment)
    1. [Logging](#logging)
1. [License](#license)

## Build

Requirements:
 * Git
 * Maven 3
 * Java 17

```bash
$ git clone <gitrepouri>
$ cd piveau-metrics-accessibility
$ mvn package
```
 
## Run

```bash
$ java -jar target/accessibility.jar
```

## Docker

Build docker image:
```bash
$ docker build -t piveau/piveau-metrics-accessibility .
```

Run docker image:
```bash
$ docker run -it -p 8080:8080 piveau/piveau-metrics-accessibility
```

## Configuration

### Environment

| Variable                                  | Description | Default Value                                                          |
|:------------------------------------------|:------------|:-----------------------------------------------------------------------|
| `PIVEAU_DCATAP_SCHEMA_CONFIG`             |             | `{}` For details, see [here](#PIVEAU_DCATAP_SCHEMA_CONFIG)             |
| `PIVEAU_ACCESSIBILITY_PERSISTENCE_CONFIG` |             | `{}` For details, see [here](#PIVEAU_ACCESSIBILITY_PERSISTENCE_CONFIG) |
| `PIVEAU_ACCESSIBILITY_ACCESS_CONFIG`      |             | `{}` For details, see [here](#PIVEAU_ACCESSIBILITY_ACCESS_CONFIG)      |

#### PIVEAU_DCATAP_SCHEMA_CONFIG

Default values:
```json
{
   "baseUri": "https://piveau.io/",
   "datasetContext": "set/data/",
   "distributionContext": "set/distribution/",
   "recordContext": "set/record/",
   "catalogueContext": "id/catalogue/",
   "metricsContext": "id/metrics/",
   "historyMetricsContext": "id/history_metrics/"
}
```

`baseUri`
: The address

`datasetContext`
: The context path for datasets, appended to the `baseUri`  

`distributionContext`
: The context path for distributions, appended to the `baseUri`

`recordContext`
: The context path for records, appended to the `baseUri`

`catalogueContext`
: The context path for catalogues, appended to the `baseUri`

`metricsContext`
: The context path for metrics graphs, appended to the `baseUri`

`historyMetricsContext`
: The context path for metrics history graphs, appended to the `baseUri`

#### PIVEAU_ACCESSIBILITY_PERSISTENCE_CONFIG

Default values:

```json
{
   "mongoDbClient": {
      "connection_string": "mongodb://localhost:27017"
   },
   "collection": "datasets"
}
```

`mongoDbClient`
: The mongo db client configuration. See [Vert.x MongoDB Client Documentation](https://vertx.io/docs/vertx-mongo-client/java/#_configuring_the_client) for more details.
The old property name `mongoDb` is still supported but deprecated. 

`collection`
: Collection to use for storing incoming pipes. If it is not existing, it will be created.

#### PIVEAU_ACCESSIBILITY_ACCESS_CONFIG

Default values:

```json
{
   "maxHeaderSize": 40960,
   "followRedirects": true,
   "maxRedirects": 2,
   "connectionTimeout": 10000,
   "requestTimeout": 10000,
   "serverCacheConfig": {
      "maxCacheEntries": 500,
      "timeToIdleExpiration": 30
   },
   "serverChannelConfig": {
      "processors": 3,
      "requestDelay": 100
   }
}
```

`maxHeaderSize`
: The maximum size in bytes of a header during the access check with HEAD and GET.

`followRedirects`
: Enable or disable following redirects.

`maxRedirects`
: Maximum number of following redirects. If you disable follow redirects completely (`"followRedirects": false`), 
you should set this to `0`. 

`connectionTimeout`
: Timeout in milliseconds to establish a connection to the server.

`requestTimeout`
: Timout in milliseconds the request should wait for a response.

`serverCacheConfig.maxCacheEntries`
: The maximum number of server channels to cache.

`serverCacheConfig.timeToIdleExpiration`
: Time in minutes before a server channel expires after last access.

`serverChannelConfig.processors`
: The numbers of server channel access checks in parallel.

`serverChannelConfig.requestDelay`
: A delay between access checks per server (and processor).

### Logging
See [logback](https://logback.qos.ch/documentation.html) documentation for more details

| Variable                   | Description                                       | Default Value                           |
|:---------------------------|:--------------------------------------------------|:----------------------------------------|
| `PIVEAU_PIPE_LOG_APPENDER` | Configures the log appender for the pipe context  | `"STDOUT"`                              |
| `PIVEAU_PIPE_LOG_PATH`     | Path to the file for the file appender            | `"logs/piveau-pipe.%d{yyyy-MM-dd}.log"` |
| `PIVEAU_PIPE_LOG_LEVEL`    | The log level for the pipe context                | `"INFO"`                                |
| `PIVEAU_LOGSTASH_HOST`     | The host of the logstash service                  | `"logstash"`                            |
| `PIVEAU_LOGSTASH_PORT`     | The port the logstash service is running          | `5044`                                  |
| `PIVEAU_LOG_LEVEL`         | The general log level for the `io.piveau` package | `"INFO"`                                |

## License

[Apache License, Version 2.0](LICENSE.md)
