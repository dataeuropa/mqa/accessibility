# ChangeLog

## Unreleased

## 2.0.2 (2024-01-02)

**Fixed:**
* Decomposition IRI to URL constructor

## 2.0.1 (2023-12-21)

**Added:**
* Support for and taking care of URLs that use IDN

## 2.0.0 (2023-11-08)

**Changed:**
* Store pipes as string and not as object

## 1.1.11 (2023-10-24)

**Added:**
* Accept header `*/*`

## 1.1.10 (2023-10-24)

**Changed:**
* Make almost everything configurable
* Set SSL always to true
* Set "trust all" to true

**Removed:**
* Unused code

**Fixed:**
* README.md

## 1.1.9 (2023-10-17)

**Changed:**
* Increased max header size
* Max redirects set to 2
* Set connection timeout

## 1.1.8 (2023-10-16)

**Fixed:**
* Register correct cache event types for expiration and eviction

**Changed:**
* Use user managed cache instead of managed cache
* Implement CacheEventListener instead of extending CacheEventAdapter

**Added:**
* Periodic access of the cache for immediate removing of expired entries

## 1.1.7 (2023-10-13)

**Removed:**
* Body encoding and storing

**Added:**
* Temp file for possible HEAD bodies

**Fixed:**
* Reset all active documents to pending during startup 

## 1.1.6 (2023-10-12)

**Fixed:**
* JSON conversion of `Status`

**Changed:**
* GET connection reset and close

## 1.1.5 (2023-07-26)

**Changed:**
* Set default job count for channel receiver to 3
* Increase server channel cache

## 1.1.4 (2023-07-14)

**Changed:**
* Simplify future handling in access module
* Decrease default job count for channel receiver 

## 1.1.3 (2023-07-11)

**Fixed:**
* Stream reset for get requests for cleanup

## 1.1.2 (2023-05-25)

**Added:**
* `method` property for `Status`

## 1.1.1 (2022-12-02)

**Changed:**
* Query for not active

## 1.1.0 (2022-12-02)

**Added:**
* Indexes for mongo db persistence

## 1.0.6 (2022-11-05)

**Fixed:**
* Queued url status completion

## 1.0.5 (2022-11-05)

**Added:**
* 10 sec timeout to requests

**Changed:**
* Invalid url or unsupported protocol are treated as successful check

**Fixed:**
* Handling of query parameters without value 

## 1.0.4 (2022-11-01)

**Fixed:**
* Set correct `dqv:computeOn`

## 1.0.3 (2022-11-01)

**Fixed:**
* Set correct `dqv:computeOn` also in case of head

## 1.0.2 (2022-11-01)

**Fixed:**
* Status URL value

## 1.0.1 (2022-09-29)

**Changed:**
* Exception handling in access handler

**Added:**
* More pipe logging

## 1.0.0 (2022-09-27)

Initial release