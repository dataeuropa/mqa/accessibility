package io.piveau.metrics.accessibility

import org.apache.jena.iri.IRIFactory

val iriFactory: IRIFactory = IRIFactory.uriImplementation()

data class RequestElems(
    val host: String,
    val port: Int,
    val path: String,
    val queryParams: Map<String, String?>,
    val ssl: Boolean,
    val file: String,
    val protocol: String
)

fun String.decomposeRequest(): RequestElems {

    val url = iriFactory.create(this).toURL()

    val queryMap = url.query?.split("&")?.map { it.split("=") }?.associate {
        when (it.size) {
            1 -> it[0] to ""
            else -> it[0] to it[1]
        }
    } ?: mapOf()

    val ssl = (url.protocol == "https")
    val port = if (url.port == -1) url.defaultPort else url.port

    return RequestElems(url.host, port, url.path, queryMap, ssl, url.file, url.protocol)
}