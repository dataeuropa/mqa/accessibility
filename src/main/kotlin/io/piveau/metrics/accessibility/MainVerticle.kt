package io.piveau.metrics.accessibility

import io.piveau.dcatap.DCATAPUriSchema
import io.piveau.metrics.accessibility.access.AccessVerticle
import io.piveau.metrics.accessibility.persistence.PersistenceVerticle
import io.piveau.metrics.accessibility.dispatcher.DispatcherVerticle
import io.piveau.metrics.accessibility.persistence.PersistenceService
import io.piveau.pipe.connector.PipeConnector
import io.piveau.pipe.model.prettyPrintJson
import io.piveau.rdf.presentAs
import io.piveau.rdf.toDataset
import io.vertx.config.ConfigRetriever
import io.vertx.core.DeploymentOptions
import io.vertx.core.Launcher
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import org.apache.jena.query.DatasetFactory
import org.apache.jena.riot.Lang
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.RDF
import org.slf4j.LoggerFactory

class MainVerticle : CoroutineVerticle() {

    private val log = LoggerFactory.getLogger(javaClass)

    override suspend fun start() {
        val config = ConfigRetriever.create(vertx).config.await()

        if (log.isDebugEnabled) log.debug(config.encodePrettily())

        DCATAPUriSchema.config = config.getJsonObject(Constants.ENV_PIVEAU_DCATAP_SCHEMA_CONFIG, JsonObject())

        vertx.deployVerticle(PersistenceVerticle::class.java, DeploymentOptions().setConfig(config)).await()
        vertx.deployVerticle(AccessVerticle::class.java, DeploymentOptions().setConfig(config)).await()
        vertx.deployVerticle(DispatcherVerticle::class.java, DeploymentOptions().setConfig(config)).await()

        val connector = PipeConnector.create(vertx).await()
        val persistenceService = PersistenceService.createProxy(vertx)
        connector.handlePipe { pipeContext ->

            // Analyse dataset here
            val dataset = when {
                pipeContext.stringData.isNotEmpty() -> {
                    pipeContext.stringData.toByteArray().toDataset(Lang.TRIG)
                }

                pipeContext.binaryData.isNotEmpty() -> {
                    pipeContext.binaryData.toDataset(Lang.TRIG)
                }

                else -> DatasetFactory.create()
            }
            val datasetModel = dataset.defaultModel

            // extract dataset metadata here
            datasetModel.listSubjectsWithProperty(RDF.type, DCAT.Dataset)
                .nextOptional()
                .ifPresentOrElse(
                    { resource ->
                        val doc = jsonObjectOf(
                            "_id" to DCATAPUriSchema.parseUriRef(resource.uri).id,
                            "priority" to Priority.HIGH,
                            "pipe" to pipeContext.pipe.prettyPrintJson()
                        )
                        persistenceService.storeObject(doc)
                    },
                    { pipeContext.setFailure("Pipe contains no dataset") }
                )
        }
    }

}

fun main(args: Array<String>) = Launcher.executeCommand("run", *(args.plus(MainVerticle::class.java.name)))
