package io.piveau.metrics.accessibility

import io.piveau.metrics.accessibility.access.AccessServiceImpl
import io.piveau.metrics.accessibility.access.QueuedURL
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.await
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicInteger

class ServerChannel(private val accessService: AccessServiceImpl, config: JsonObject = JsonObject()) {

    private val log = LoggerFactory.getLogger(javaClass)

    private val channel = Channel<QueuedURL>()

    var queued = AtomicInteger()

    private var processors = config.getInteger("processors", 3)
    private var requestDelay = config.getLong("requestDelay", 100)

    private val jobs = mutableListOf<Job>()

    init {
        start()
    }

    suspend fun reconfigure(config: JsonObject) {
        stop()
        requestDelay = config.getLong("requestDelay", requestDelay)
        processors = config.getInteger("processors", processors)
        start()
    }

    private fun start() {
        if (processors < 1) processors = 1
        repeat(processors) {
            jobs.add(accessService.coroutineScope.launch { process() })
        }
    }

    private suspend fun stop() {
        jobs.onEach { it.cancelAndJoin() }.clear()
    }

    private suspend fun process() {
        for (queuedUrl in channel) {
            try {
                queued.decrementAndGet()
                val status = accessService.accessUrl(queuedUrl.url).await()
                queuedUrl.complete(status)
            } catch (e: Exception) {
                queuedUrl.fail(e)
            }
            if (requestDelay > 0) delay(requestDelay)
        }
    }

    suspend fun send(queuedUrl: QueuedURL) = queued.incrementAndGet().also { channel.send(queuedUrl) }

    fun shutdown() {
        try {
            channel.close()
        } catch (e: Exception) {
            log.error("Closing channel", e)
        }
    }

}