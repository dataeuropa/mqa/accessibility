package io.piveau.metrics.accessibility.access

import io.piveau.metrics.accessibility.Constants
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.serviceproxy.ServiceBinder

class AccessVerticle : CoroutineVerticle() {

    override suspend fun start() {
        val accessConfig = config.getJsonObject(Constants.ENV_PIVEAU_ACCESSIBILITY_ACCESS_CONFIG, JsonObject())
        val service = AccessService.create(vertx, accessConfig, this).await()
        ServiceBinder(vertx).setAddress(AccessService.ADDRESS).register(AccessService::class.java, service)
    }

}