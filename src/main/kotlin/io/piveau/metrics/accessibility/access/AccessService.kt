package io.piveau.metrics.accessibility.access

import io.vertx.codegen.annotations.GenIgnore
import io.vertx.codegen.annotations.ProxyGen
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.eventbus.DeliveryOptions
import io.vertx.core.json.JsonObject
import kotlinx.coroutines.CoroutineScope

@ProxyGen
interface AccessService {

    fun queueUrl(urlInfo: JsonObject): Future<JsonObject>

    fun accessUrl(url: String): Future<JsonObject>

    @GenIgnore
    companion object {
        const val ADDRESS = "io.piveau.metrics.accessibility.access.service"

        fun create(vertx: Vertx, accessConfig: JsonObject, coroutineScope: CoroutineScope): Future<AccessService> =
            Future.succeededFuture(AccessServiceImpl(vertx, accessConfig, coroutineScope))

        fun createProxy(vertx: Vertx) = AccessServiceVertxEBProxy(vertx, ADDRESS, DeliveryOptions().setSendTimeout(1800000))
    }

}