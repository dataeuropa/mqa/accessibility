package io.piveau.metrics.accessibility.access

import io.piveau.metrics.accessibility.*
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.core.Vertx
import io.vertx.core.file.OpenOptions
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.http.HttpHeaders
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.RequestOptions
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.WebClientOptions
import io.vertx.ext.web.codec.BodyCodec
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.apache.commons.validator.routines.UrlValidator
import org.ehcache.Cache
import org.ehcache.config.builders.*
import org.ehcache.config.units.EntryUnit
import org.ehcache.event.*
import org.slf4j.LoggerFactory
import java.time.Duration
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicReference


operator fun <K, V> Cache.Entry<K, V>.component1(): K = key
operator fun <K, V> Cache.Entry<K, V>.component2(): V = value

internal val allowedProtocols = listOf("http", "https")

data class QueuedURL(val urlInfo: JsonObject, val promise: Promise<JsonObject>) {
    val url: String = urlInfo.getString("uri")
    fun complete(status: JsonObject) = promise.complete(urlInfo.put("status", status))
    fun fail(t: Throwable) = promise.fail(t)
}

class ExpiringListener : CacheEventListener<String, ServerChannel> {
    private val logger = LoggerFactory.getLogger(javaClass)

    private val eventList = listOf(EventType.EXPIRED, EventType.EVICTED)

    override fun onEvent(event: CacheEvent<out String, out ServerChannel>) {
        logger.debug("Channel {} {}", event.key, event.type.name)
        if (event.type in eventList) {
            event.oldValue?.shutdown()
        }
    }

}

class AccessServiceImpl(
    val vertx: Vertx,
    private val accessConfig: JsonObject,
    val coroutineScope: CoroutineScope
) : AccessService {

    private val coreClient = vertx.createHttpClient(
        HttpClientOptions()
            .setMaxHeaderSize(accessConfig.getInteger("maxHeaderSize", 40960))
            .setMaxRedirects(accessConfig.getInteger("maxRedirects", 2))
            .setConnectTimeout(accessConfig.getInteger("connectionTimeout", 10000))
            .setSsl(true)
            .setTrustAll(true)
    )
    private val webClient = WebClient.create(
        vertx, WebClientOptions()
            .setMaxHeaderSize(accessConfig.getInteger("maxHeaderSize", 40960))
            .setMaxRedirects(accessConfig.getInteger("maxRedirects", 2))
            .setConnectTimeout(accessConfig.getInteger("connectionTimeout", 10000))
            .setSsl(true)
            .setTrustAll(true)
    )

    private val urlValidator = UrlValidator(UrlValidator.ALLOW_2_SLASHES)

    private val cacheConfig = accessConfig.getJsonObject("serverCacheConfig", JsonObject())

    private val userManagedCache =
        UserManagedCacheBuilder.newUserManagedCacheBuilder(String::class.java, ServerChannel::class.java)
            .withResourcePools(
                ResourcePoolsBuilder.newResourcePoolsBuilder().heap(
                    cacheConfig.getLong("maxCacheEntries", 500), EntryUnit.ENTRIES
                )
            )
            .withEventExecutors(Executors.newSingleThreadExecutor(), Executors.newFixedThreadPool(5))
            .withEventListeners(
                CacheEventListenerConfigurationBuilder
                    .newEventListenerConfiguration(ExpiringListener(), EventType.EXPIRED, EventType.EVICTED)
                    .asynchronous()
                    .unordered()
            )
            .withExpiry(
                ExpiryPolicyBuilder.timeToIdleExpiration(
                    Duration.ofMinutes(
                        cacheConfig.getLong("timeToIdleExpiration", 30)
                    )
                )
            )
            .build(true)

    init {
        vertx.setPeriodic(30000) {
            // Flushing expired and evicted entries...
            userManagedCache.count()
        }
    }

    override fun queueUrl(urlInfo: JsonObject): Future<JsonObject> = Future.future { promise ->
        try {
            val queuedUrl = QueuedURL(urlInfo, promise)

            if (!urlValidator.isValid(queuedUrl.url)) {
                queuedUrl.complete(Status.create(Throwable("Invalid URL")).asJsonObject(queuedUrl.url))
                return@future
            }

            val requestElems = queuedUrl.url.decomposeRequest()

            if (requestElems.protocol !in allowedProtocols) {
                queuedUrl.complete(
                    Status.create(Throwable("Protocol ${requestElems.protocol} not supported"))
                        .asJsonObject(queuedUrl.url)
                )
                return@future
            }

            val serverChannel = userManagedCache[requestElems.host]
                ?: ServerChannel(this, accessConfig.getJsonObject("serverChannelConfig", JsonObject()))
                    .apply {
                        userManagedCache.put(requestElems.host, this)
                    }

            coroutineScope.launch {
                serverChannel.send(queuedUrl)
            }
        } catch (t: Throwable) {
            promise.fail(Throwable("Exception while queueing access check for ${urlInfo.getString("uri")}: ${t.message}"))
        }
    }

    override fun accessUrl(url: String): Future<JsonObject> {
        return headRequest(url)
            .compose { status ->
                status.put("url", url)
                when {
                    status.getBoolean("response") && status.getInteger("code") == 405 -> getRequest(url)
                    else -> Future.succeededFuture(status)
                }
            }
    }

    private fun headRequest(url: String): Future<JsonObject> {
        val (host, port, path, queryParams, _) = try {
            url.decomposeRequest()
        } catch (t: Throwable) {
            return Future.succeededFuture(Status.create(t, HttpMethod.HEAD).asJsonObject(url))
        }

        val request = webClient.head(port, host, path)
        queryParams.forEach { (key, value) -> request.addQueryParam(key, value) }

        val ref = AtomicReference("")
        return vertx.fileSystem().createTempFile("piveau_", null)
            .compose { fileName ->
                ref.set(fileName)
                vertx.fileSystem().open(fileName, OpenOptions().setWrite(true))
            }
            .compose { stream ->
                request
                    .timeout(accessConfig.getLong("requestTimeout", 10000))
                    .`as`(BodyCodec.pipe(stream, true))
                    .putHeader(HttpHeaders.ACCEPT.toString(), "*/*")
                    .send()
            }
            .map { Status.create(it).asJsonObject(url) }
            .otherwise { Status.create(it, HttpMethod.HEAD).asJsonObject(url) }
            .onComplete {
                if (ref.get().isNotBlank()) vertx.fileSystem().delete(ref.get())
            }
    }

    private fun getRequest(url: String): Future<JsonObject> {
        val (host, port, _, _, _, file) = try {
            url.decomposeRequest()
        } catch (t: Throwable) {
            return Future.succeededFuture(Status.create(t, HttpMethod.GET).asJsonObject(url))
        }

        val options = RequestOptions()
        with(options) {
            this.host = host
            this.port = port
            isSsl = true
            method = HttpMethod.GET
            uri = file
            timeout = accessConfig.getLong("requestTimeout", 10000)
        }

        return coreClient.request(options)
            .compose { it.connect() }
            .map { response ->
                response.request().reset()
                response.request().connection().close()
                Status.create(response).asJsonObject(url)
            }
            .otherwise { Status.create(it, HttpMethod.GET).asJsonObject(url) }
    }

}