package io.piveau.metrics.accessibility.persistence

import io.piveau.metrics.accessibility.Constants
import io.vertx.core.json.JsonObject
import io.vertx.ext.mongo.IndexOptions
import io.vertx.ext.mongo.MongoClient
import io.vertx.ext.mongo.UpdateOptions
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.serviceproxy.ServiceBinder
import org.slf4j.LoggerFactory

class PersistenceVerticle : CoroutineVerticle() {

    private lateinit var mongoClient: MongoClient

    private val defaultMongoDbConfig = JsonObject().put("connection_string", "mongodb://localhost:27017")

    private val log = LoggerFactory.getLogger(javaClass)

    override suspend fun start() {
        val persistenceConfig =
            config.getJsonObject(Constants.ENV_PIVEAU_ACCESSIBILITY_PERSISTENCE_CONFIG, JsonObject())

        mongoClient = MongoClient.createShared(vertx, persistenceConfig.getJsonObject("mongoDbClient",
            persistenceConfig.getJsonObject("mongoDb", defaultMongoDbConfig)))

        val collection = persistenceConfig.getString("collection", "datasets")

        val indexes = mongoClient.listIndexes(collection).await()
            .filterIsInstance(JsonObject::class.java)
            .mapNotNull { it.getString("name") }

        if (!indexes.contains("Status-Timestamp")) {
            mongoClient.createIndexWithOptions(
                collection, JsonObject()
                    .put("status", -1)
                    .put("timestamp", 1), IndexOptions().name("Status-Timestamp")
            )
        }
        if (!indexes.contains("Status")) {
            mongoClient.createIndexWithOptions(
                collection, JsonObject()
                    .put("status", 1), IndexOptions().name("Status")
            )
        }

        val service = PersistenceService.create(mongoClient, persistenceConfig).await()
        ServiceBinder(vertx).setAddress(PersistenceService.ADDRESS).register(PersistenceService::class.java, service)

        val reset = mongoClient
            .updateCollectionWithOptions(
                collection,
                PersistenceServiceImpl.activeQuery,
                PersistenceServiceImpl.pendingUpdate,
                UpdateOptions().setMulti(true)
            )
            .await().toJson()

        if (log.isDebugEnabled) log.debug("Reset: ${reset.encodePrettily()}")
    }

}
