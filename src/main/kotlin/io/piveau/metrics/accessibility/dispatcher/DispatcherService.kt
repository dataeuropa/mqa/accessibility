package io.piveau.metrics.accessibility.dispatcher

import io.piveau.metrics.accessibility.access.AccessService
import io.piveau.metrics.accessibility.persistence.PersistenceService
import io.vertx.codegen.annotations.GenIgnore
import io.vertx.codegen.annotations.ProxyGen
import io.vertx.core.CompositeFuture
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.eventbus.DeliveryOptions
import io.vertx.core.json.JsonObject

/**
 *
 */
@ProxyGen
interface DispatcherService {

    /**
     *
     */
    fun dispatchObject(obj: JsonObject): Future<Void>

    @GenIgnore
    companion object {
        const val ADDRESS = "io.piveau.metrics.accessibility.scheduler.service"

        fun create(vertx: Vertx, accessService: AccessService): Future<DispatcherService> = Future.future { promise ->
            promise.complete(DispatcherServiceImpl(vertx, accessService))
        }

        fun createProxy(vertx: Vertx) = DispatcherServiceVertxEBProxy(vertx, ADDRESS, DeliveryOptions())
    }

}