package io.piveau.metrics.accessibility.dispatcher

import io.piveau.metrics.accessibility.access.AccessService
import io.piveau.rdf.toModel
import io.vertx.core.json.JsonObject
import org.apache.jena.rdf.model.Model

class DatasetDispatcher(obj: JsonObject, accessService: AccessService) : Dispatcher(obj, accessService) {
    override fun extractModel(): Model {
        val mimeType = obj.getString("mimeType")
        return obj.getString("dataset").toByteArray().toModel(mimeType)
    }

    override fun complete() {
        TODO("Not yet implemented")
    }
}
