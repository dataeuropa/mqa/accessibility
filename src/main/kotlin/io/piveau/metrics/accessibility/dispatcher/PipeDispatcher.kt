package io.piveau.metrics.accessibility.dispatcher

import io.piveau.dqv.addMeasurement
import io.piveau.dqv.listMetricsModels
import io.piveau.dqv.removeMeasurement
import io.piveau.metrics.accessibility.access.AccessService
import io.piveau.pipe.ForwardVerticle
import io.piveau.pipe.PipeContext
import io.piveau.pipe.PipeManager
import io.piveau.rdf.*
import io.piveau.vocabularies.vocabulary.PV
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import org.apache.jena.rdf.model.Model
import org.apache.jena.riot.Lang
import org.apache.jena.vocabulary.DCAT

class PipeDispatcher(obj: JsonObject, accessService: AccessService, private val vertx: Vertx) : Dispatcher(obj, accessService) {

    private val manager = when (val value = obj.getValue("pipe")) {
        is String -> PipeManager.read(value)
        is JsonObject -> PipeManager.read(value.encode())
        else -> PipeManager.read("")
    }

    private val jenaDataset = manager.data.toByteArray().toDataset(manager.mimeType?.asRdfLang() ?: Lang.TRIG)

    private val pipeContext = PipeContext(manager.pipe)

    override fun extractModel(): Model = jenaDataset.defaultModel

    private val metricsModel: Model by lazy {
        jenaDataset.listMetricsModels()[0]
    }

    override fun processSucceeded(succeededFutures: List<Future<JsonObject>>) {
        succeededFutures.forEach { future ->
            val result = future.result()

            val url = metricsModel.getResource(result.getString("uri"))
            val distribution = metricsModel.getResource(result.getString("distributionUri"))
            val metric = when(result.getString("property")) {
                DCAT.downloadURL.uri -> PV.downloadUrlStatusCode
                else -> PV.accessUrlStatusCode
            }

            if (log.isDebugEnabled) {
                log.debug(result.encodePrettily())
            }

            val status = result.getJsonObject("status")
            val value = when (status.getBoolean("response")) {
                true -> status.getInteger("code")
                else -> 1100
            }

            pipeContext.log.debug("Access check successful for $url: $value")

            metricsModel.removeMeasurement(distribution, metric, url)
            metricsModel.addMeasurement(distribution, url, metric, value)
        }
    }

    override fun processFailed(failedFutures: List<Future<JsonObject>>) {
        failedFutures.forEach {
            pipeContext.log.warn("Access check failed for ${obj.getString("_id")}: ${it.cause().message}")
        }
    }

    override fun complete() {
        pipeContext.log.info("Accessibility checked for ${manager.dataInfo}")
        val content = jenaDataset.presentAs(Lang.TRIG)
        pipeContext.log.trace(content)
        pipeContext.setResult(content, RDFMimeTypes.TRIG, manager.dataInfo)
        vertx.eventBus().send(ForwardVerticle.ADDRESS, pipeContext.getFinalBuffer())
    }

}
