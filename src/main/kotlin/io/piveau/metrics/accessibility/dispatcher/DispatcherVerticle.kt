package io.piveau.metrics.accessibility.dispatcher

import io.piveau.metrics.accessibility.access.AccessService
import io.piveau.metrics.accessibility.persistence.PersistenceService
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.serviceproxy.ServiceBinder
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.flow
import org.slf4j.LoggerFactory

class DispatcherVerticle : CoroutineVerticle() {

    private lateinit var job: Job

    private val log = LoggerFactory.getLogger(javaClass)

    override suspend fun start() {
        val service = DispatcherService.create(
            vertx,
            AccessService.createProxy(vertx)
        ).await()
        ServiceBinder(vertx).setAddress(DispatcherService.ADDRESS).register(DispatcherService::class.java, service)

        job = launch {
            val persistenceService = PersistenceService.createProxy(vertx)
            val dispatchService = DispatcherService.createProxy(vertx)
            flow {
                while (this@launch.isActive) {
                    delay(500)
                    try {
                        do {
                            val nextObjs = persistenceService.nextObjects().await()
                            nextObjs.forEach { emit(it) }
                        } while (nextObjs.isNotEmpty() && this@launch.isActive)
                    } catch (e: CancellationException) {
                        log.debug("Dataset flow job canceled")
                    } catch (t: Throwable) {
                        log.error("Dataset flow emitting", t)
                    }
                }
            }.collect { obj ->
                try {
                    dispatchService.dispatchObject(obj)
                        .onComplete {
                            persistenceService.removeObject(obj.getString("_id"))
                        }
                } catch (e: CancellationException) {
                    log.debug("Dataset flow job canceled")
                } catch (t: Throwable) {
                    log.error("Dataset flow collecting", t)
                }
            }
        }
    }

    override suspend fun stop() {
        job.cancelAndJoin()
    }

}