package io.piveau.metrics.accessibility.dispatcher

import io.piveau.metrics.accessibility.access.AccessService
import io.piveau.metrics.accessibility.persistence.PersistenceService
import io.vertx.core.CompositeFuture
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject

class DispatcherServiceImpl(
    private val vertx: Vertx,
    private val accessService: AccessService
) : DispatcherService {

    override fun dispatchObject(obj: JsonObject): Future<Void> {
        val dispatcher = try {
            when {
                obj.containsKey("pipe") -> PipeDispatcher(obj, accessService, vertx)
                obj.containsKey("dataset") -> DatasetDispatcher(obj, accessService)
                else -> NopeDispatcher(obj, accessService)
            }
        } catch (e: Exception) {
            return Future.failedFuture(e)
        }

        return dispatcher.dispatch().transform { Future.succeededFuture() }
    }

}
