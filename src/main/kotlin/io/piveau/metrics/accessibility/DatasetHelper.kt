package io.piveau.metrics.accessibility

import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.jsonObjectOf
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Statement
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms

data class Distribution(val accessURLs: List<JsonObject>, val downloadURLs: List<JsonObject>, val formatExists: Boolean)

fun Model.datasetDistributions(): List<Distribution> {
    return listObjectsOfProperty(DCAT.distribution)
        .filterKeep { it.isResource }
        .mapWith { it.asResource() }
        .mapWith { distribution ->
                val formatExists =
                    distribution.hasProperty(DCTerms.format) || distribution.hasProperty(DCAT.mediaType)
                val accessURLs = distribution.listProperties(DCAT.accessURL)
                    .filterKeep { it.`object`.isURIResource }
                    .mapWith { it.asJsonObject() }.toList()
                val downloadURLs = distribution.listProperties(DCAT.downloadURL)
                    .filterKeep { it.`object`.isURIResource }
                    .mapWith { it.asJsonObject() }.toList()

                Distribution(accessURLs, downloadURLs, formatExists)
            }
        .toList()
}

private fun Statement.asJsonObject() = jsonObjectOf(
    "distributionUri" to subject.uri,
    "property" to predicate.uri,
    "uri" to `object`.asResource().uri
)
