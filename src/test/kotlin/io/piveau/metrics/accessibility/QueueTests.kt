package io.piveau.metrics.accessibility

import io.piveau.dcatap.DCATAPUriSchema
import io.piveau.dcatap.TripleStore
import io.piveau.metrics.accessibility.persistence.PersistenceService
import io.piveau.rdf.RDFMimeTypes
import io.piveau.rdf.asString
import io.vertx.core.DeploymentOptions
import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.await
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import org.apache.jena.riot.Lang
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith

@DisplayName("Queue tests")
@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Disabled
class QueueTests {

    @BeforeAll
    fun setUp(vertx: Vertx, testContext: VertxTestContext) {
        vertx.deployVerticle(MainVerticle::class.java, DeploymentOptions())
            .onSuccess { testContext.completeNow() }
            .onFailure(testContext::failNow)
    }

    @Test
    fun `Testing intermediate persistence`(vertx: Vertx, testContext: VertxTestContext) = runBlocking {
        val catalogueId = "govdata"

        DCATAPUriSchema.config = jsonObjectOf(
            "baseUri" to "http://data.europa.eu/88u/",
            "datasetContext" to "dataset/",
            "distributionContext" to "distribution/",
            "recordContext" to "record/",
            "catalogueContext" to "catalogue/",
            "metricsContext" to "metrics/",
            "historyMetricsContext" to "history_metrics/"
        )

        val tripleStore = TripleStore(vertx, jsonObjectOf("address" to "https://data.europa.eu"))

        val datasets = tripleStore.catalogueManager.allDatasets(catalogueId).await()
        println("Found ${datasets.size} datasets in $catalogueId")

        val persistenceService = PersistenceService.createProxy(vertx)

        datasets
            .take(100)
            .asFlow()
            .map { it to tripleStore.datasetManager.getGraph(it.graphNameRef).await() }
            .collect { (uriRef, model) ->
                val obj = jsonObjectOf(
                    "_id" to uriRef.id,
                    "priority" to Priority.NORMAL,
                    "dataset" to model.asString(Lang.NTRIPLES),
                    "mimeType" to RDFMimeTypes.NTRIPLES
                )
                persistenceService.storeObject(obj).await()
            }

        println("All objects stored")
        delay(50000)

        testContext.completeNow()
    }

}