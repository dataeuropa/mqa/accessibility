package io.piveau.metrics.accessibility

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Disabled
import kotlin.test.Test

@Disabled
class FlowTests {

    @Test
    fun `Some experiments`() = runBlocking {

        launch {
            flow {
                listOf(1, 2, 3, 4, 5, 6, 7).forEach {
                    println("emitting $it")
                    emit(it)
                    println("$it emitted")
                }
            }
                .buffer(1)
                .collect {
                    println("$it collected")
                    delay(2000)
                    println("collection processed")
                }
        }

        println("End")
    }

}